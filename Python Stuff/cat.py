import pygame, sys 
from pygame.locals import *

pygame.init()

FPS = 120 #frames per second setting
fpsClock = pygame.time.Clock()

#set up the window
DISPLAYSURF = pygame.display.set_mode()
pygame.display.set_caption('Animation')


WHITE = (255, 255, 255)
catImg = pygame.image.load('cat.png')
catx = 10
caty = 10 
direction = 'right'


while True: #the main game loop
	DISPLAYSURF.fill(WHITE)
	pygame.draw.rect(DISPLAYSURF, BLACK, ( 960, 590, boxx, boxy)
	
	
	if direction == 'right':
		catx += 5
		if catx == 280:
			direction = 'down'
	elif direction == 'down':
		caty+= 5
		if caty == 220:
			direction = 'left'
	elif direction == 'left':
		catx -= 5
		if catx == 10:
			direction = 'up'
	elif direction == 'up':
		caty -= 5
		if caty == 10:
			direction = 'right'

	DISPLAYSURF.blit(catImg, (catx, caty))

	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()

	pygame.display.update()
	fpsClock.tick(FPS)
