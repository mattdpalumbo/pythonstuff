import urllib.request, json


#getjson
with urllib.request.urlopen("https://api.coinmarketcap.com/v1/ticker/?limit=1000") as url:
    data = json.loads(url.read().decode())
  
#define emtpy dictionarie sfor coin data    
vtc = {}
btc = {}
ltc = {}
xlm = {}
ethos = {}
neo = {}
nxs = {}
eth = {}
sc = {}
enj = {}
abj = {}

#parse json to define above dictionaries
for x in data:
	if x['id'] == 'abjcoin':
		abj = x

	if x['id'] == 'enjin-coin':
		enj = x

	if x['id'] == 'bitcoin':
		btc = x

	if x['id'] == 'ethos':
		ethos = x

	if x['id'] == 'vertcoin':
		vtc = x

	if x['id'] == 'litecoin':
		ltc = x

	if x['id'] == 'stellar':
		xlm = x

	if x['id'] == 'neo':
		neo = x

	if x['id'] == 'nexus':
		nxs = x

	if x['id'] == 'ethereum':
		eth = x

	if x['id'] == 'siacoin':
		sc = x

#where you input your coin balance
enjamount = 420
abjamount = 0
vtcamount = 29.51
btcamount  = 0.00303815
ltcamount  = 0.01842992
xlmamount  = 243.400
ethosamount  = 39.9505226
neoamount  = 0.0
nxsamount  = 25.0
ethamount  = 0.0223395
scamount  = 6918.373

#where you define the coins price
enjprice = float(enj["price_usd"])
abjprice = float(abj["price_usd"])
vtcprice = float(vtc["price_usd"])
btcprice = float(btc["price_usd"])
ltcprice = float(ltc["price_usd"])
xlmprice = float(xlm["price_usd"])
ethosprice = float(ethos["price_usd"])
neoprice = float(neo["price_usd"])
nxsprice = float(nxs["price_usd"])
ethprice = float(eth["price_usd"])
scprice = float(sc["price_usd"])

#your total current balance
my_balance = float((vtcprice * vtcamount) 
	+ (btcamount * btcprice) 
	+ (ltcamount * ltcprice)
	+ (xlmamount * xlmprice)
	+ (ethosamount * ethosprice)
	+ (neoprice * neoamount)
	+ (nxsamount * nxsprice)
	+ (ethprice * ethamount)
	+ (scprice * scamount)
	+ (enjprice * enjamount)
	)

#your total investment amount in USD
initial_capital = 10.00 + 20.00 + 50.00 + 100.00  + 100.00 + 50.00 + 45.00 + 78.00 + 50.00 + 59.77 + 59.70 + 50.00 + 150 

#your break even on investment withdrawn
withdrawn_usd = 517.18 + 83.00 + 75.01 + 147.28

#this capital is how much you're in the hole
capital = initial_capital - withdrawn_usd

#these are obvious
percent_change = ((my_balance/capital) * 100.00) -  100
current_profit = my_balance - capital
in_the_clear = initial_capital - withdrawn_usd

#print data
print(("My balance is:"), my_balance)
print(("My initial capital was:"), initial_capital)
print("My total withdrwals in USD have been: $", withdrawn_usd)
print(("My current net invested capital is:"), format(capital, '.2f'))
print("")
print (("My current profit not considering withdrawn USD is:"), current_profit)

print ("")
if capital < 0.01:
	print ("The percent change is: INIFINITY, EVERYTHING IS PROFIT BOYEEEEE!!!!!")
else:
	print (("The percent change is: "), percent_change)


print ("")
print  ("BTC value: $", (btcamount * btcprice))
print  ("VTC value: $", (vtcamount * vtcprice))
print ("LTC value: $", (ltcamount * ltcprice))
print ("XLM value: $", (xlmamount * xlmprice))
print ("Ethos value: $", (ethosamount * ethosprice))
print ("NEO value: $", (neoprice * neoamount))	
print ("NXS value: $", (nxsamount * nxsprice))
print ("ETH value: $", (ethprice * ethamount))
print ("SC value: $", (scprice * scamount))
print ("ENJ value: $", (enjprice * enjamount))
print ("")
print ("I have withdrawn, ", withdrawn_usd," in USD. That leaves $", format(in_the_clear, ".2f"), " until I'm in the clear and everything is profit")
print("")
print("")
print("Put Calclulations down here bub:")
print("")
