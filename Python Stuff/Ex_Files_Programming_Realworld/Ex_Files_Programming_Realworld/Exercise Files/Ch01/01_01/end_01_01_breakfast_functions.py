""" A Functional Breakfast """

def make_omelette():  #in
    print('Mixing the ingredients')
    print('Pouring the mixture into a frying pan')
    print('Cooking the first side')
    print('Flipping it!')
    print('Cooking the other side')
    omelette = 'a tasty omelette'
    return omelette #what comes out - we are returning the product of this, which is omelette

# make two omelettes
omelette1 = make_omelette()
omelette2 = make_omelette()

print(omelette1)
print(omelette2)

print(make_omelette() ) * 10