import urllib.request, json
import numpy as np
import pandas as pd
from pandas_datareader import data as wb
import matplotlib.pyplot as plt
import datetime

#get number of days since first investment date
today = datetime.date.today()
first_day_invested = datetime.date(2017, 10, 30)
days_since_invested = today - first_day_invested

#print (days_since_invested)

#get jsons

		#btc
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym=USD&limit=%s" % days_since_invested.days) as url:
    btc = json.loads(url.read().decode())

		#ltc
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=LTC&tsym=USD&limit=%s" % days_since_invested.days) as url:
    ltc = json.loads(url.read().decode())

		#vtc
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=VTC&tsym=USD&limit=%s" % days_since_invested.days) as url:
    vtc = json.loads(url.read().decode())

		#ETH
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=ETH&tsym=USD&limit=%s" % days_since_invested.days) as url:
    eth = json.loads(url.read().decode())

		#XLM
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=XLM&tsym=USD&limit=%s" % days_since_invested.days) as url:
    xlm = json.loads(url.read().decode())

		#sc
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=SC&tsym=USD&limit=%s" % days_since_invested.days) as url:
    sc = json.loads(url.read().decode())

		#NXS
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=NXS&tsym=USD&limit=%s" % days_since_invested.days) as url:
    nxs = json.loads(url.read().decode())

		#enj
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=ENJ&tsym=USD&limit=%s" % days_since_invested.days) as url:
    enj = json.loads(url.read().decode())

		#ethos
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=BQX&tsym=USD&limit=%s" % days_since_invested.days) as url:
    ethos = json.loads(url.read().decode())


#get JUST the data, not the response confirmation
btcdata = btc['Data']
ltcdata = ltc['Data']
vtcdata = vtc['Data']
ethdata = eth['Data']
xlmdata = xlm['Data']
scdata = sc['Data']
nxsdata = nxs['Data']
enjdata = enj['Data']
ethosdata = ethos['Data']

#make empty lists for coin prices
btcpricelist = []
ltcpricelist = []
vtcpricelist = []
ethpricelist = []
xlmpricelist = []
scpricelist = []
nxspricelist = []
enjpricelist = []
ethospricelist = []



#append prices to empty lists
for x in range(0,days_since_invested.days):
	btcpricelist.append(btcdata[x]['close'])
	ltcpricelist.append(ltcdata[x]['close'])
	vtcpricelist.append(vtcdata[x]['close'])
	ethpricelist.append(ethdata[x]['close'])
	xlmpricelist.append(xlmdata[x]['close'])
	scpricelist.append(scdata[x]['close'])
	nxspricelist.append(nxsdata[x]['close'])
	enjpricelist.append(enjdata[x]['close'])
	ethospricelist.append(ethosdata[x]['close'])

#currentprices
btcprice = btcpricelist[days_since_invested.days - 1]
ltcprice = ltcpricelist[days_since_invested.days - 1]
vtcprice = vtcpricelist[days_since_invested.days - 1]
ethprice = ethpricelist[days_since_invested.days - 1]
xlmprice = xlmpricelist[days_since_invested.days - 1]
scprice = scpricelist[days_since_invested.days - 1]
nxsprice = nxspricelist[days_since_invested.days - 1]
enjprice = enjpricelist[days_since_invested.days - 1]
ethosprice = ethospricelist[days_since_invested.days - 1]

#add price lists to dictionaries for graphing

datalist = {}
datalist["BTC"] = btcpricelist
datalist["LTC"] = ltcpricelist
datalist["VTC"] = vtcpricelist
datalist["ETH"] = ethpricelist
datalist["XLM"] = xlmpricelist
datalist["SC"] = scpricelist
datalist["NXS"] = nxspricelist
datalist["ENJ"] = enjpricelist
datalist["ETHOS"] = ethospricelist	

mydata = pd.DataFrame(data=datalist)

(mydata/mydata.iloc[0]*100).plot(figsize=(25, 10));
plt.xlabel('Days Since Investment')
plt.ylabel('Precent Change')
plt.title('Daily Percent Change in Coins')
#plt.show()

sec_returns = np.log(mydata/mydata.shift(1))
print('BTC mean daily change: ',sec_returns['BTC'].mean()* 250 )
print('LTC mean daily change: ',sec_returns['LTC'].mean()* 250 )
print('VTC mean daily change: ',sec_returns['VTC'].mean()* 250 )
print('ETH mean daily change: ',sec_returns['ETH'].mean()* 250 )
print('XLM mean daily change: ',sec_returns['XLM'].mean()* 250 )
print('SC mean daily change: ',sec_returns['SC'].mean()* 250 )
print('NXS mean daily change: ',sec_returns['NXS'].mean()* 250 )
print('ENJ mean daily change: ',sec_returns['ENJ'].mean()* 250 )
print('ETHOS mean daily change: ', sec_returns['ETHOS'].mean()* 250 )


#where you input your coin balance
enjamount = 420.00
abjamount = 0.00
vtcamount = 29.51
btcamount  = 0.00303815
ltcamount  = 0.01842992
xlmamount  = 243.400
ethosamount  = 39.9505226
neoamount  = 0.0
nxsamount  = 25.0
ethamount  = 0.0223395
scamount  = 7428.15

#your total current balance
my_balance = float((vtcprice * vtcamount) 
	+ (btcamount * btcprice) 
	+ (ltcamount * ltcprice)
	+ (xlmamount * xlmprice)
	+ (ethosamount * ethosprice)
	+ (nxsamount * nxsprice)
	+ (ethprice * ethamount)
	+ (scprice * scamount)
	+ (enjprice * enjamount)
	)

#your total investment amount in USD
initial_capital = 10.00 + 20.00 + 50.00 + 100.00  + 100.00 + 50.00 + 45.00 + 78.00 + 50.00 + 59.77 + 59.70 + 50.00 + 150 

#your break even on investment withdrawn
withdrawn_usd = 517.18 + 83.00 + 75.01 + 147.28

#this capital is how much you're in the hole
capital = initial_capital - withdrawn_usd

#these are obvious
percent_change = ((my_balance/capital) * 100.00) -  100
current_profit = my_balance - capital
in_the_clear = initial_capital - withdrawn_usd

#print data
print(("My balance is:"), my_balance)
print(("My initial capital was:"), initial_capital)
print("My total withdrwals in USD have been: $", withdrawn_usd)
print(("My current net invested capital is:"), format(capital, '.2f'))
print("")
print (("My current profit not considering profit withdrawn USD is:"), current_profit)

print ("")
if capital < 0.01:
	print ("The percent change is: INIFINITY, EVERYTHING IS PROFIT BOYEEEEE!!!!!")
else:
	print (("The percent change is: "), percent_change)


print ("")
print  ("BTC value: $", (btcamount * btcprice))
print  ("VTC value: $", (vtcamount * vtcprice))
print ("LTC value: $", (ltcamount * ltcprice))
print ("XLM value: $", (xlmamount * xlmprice))
print ("Ethos value: $", (ethosamount * ethosprice))
#print ("NEO value: $", (neoprice * neoamount))	
print ("NXS value: $", (nxsamount * nxsprice))
print ("ETH value: $", (ethprice * ethamount))
print ("SC value: $", (scprice * scamount))
print ("ENJ value: $", (enjprice * enjamount))
print ("")
print ("I have withdrawn, ", withdrawn_usd," in USD. That leaves $", format(in_the_clear, ".2f"), " until I'm in the clear and everything is profit")
print("")
print("")
print("Put Calclulations down here bub:")
print("")