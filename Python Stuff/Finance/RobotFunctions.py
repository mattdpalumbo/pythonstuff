
# coding: utf-8

# In[2]:


import urllib.request, json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime

#import BittrexMarkets as bm
import BittrexCoins as bc
# import jtplot submodule from jupyterthemes


# currently installed theme will be used to
# set plot style if no arguments provided
# In[3]:


import time


# In[4]:


coins = []
for x in bc.coins_names:
    coins.append(str(x))
print(coins)


# In[8]:


all_coin_data = {}
for x in coins:
    time.sleep(1)
    with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=%s&tsym=USD&allData=true&" % str(x)) as url:
        data = json.loads(url.read().decode())
    all_coin_data[str(x)] = data['Data']



# In[9]:


print (all_coin_data)


# In[ ]:


"""def main():
    #get coins history on the market start with USD values will start with day, add which history feature later, as well as conversion symbol
    def get_all_coins_data():
        for x in coins:
            with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=%s&tsym=USD&allData=true&" % str(x)) as url:
                data = json.loads(url.read().decode())
            all_coin_data[str(x)] = data['Data']
        return all_coin_data
    get_all_coins_data()
    return all_coin_data"""


# In[ ]:



#get JUST the data, not the response confirmation

btc_data = BTC["Data"]

#make the time unix to 24hr est
for x in range(0, len(btc_data)-1):
    btc_data[x]['time'] = time.strftime("%D %H:%M", time.localtime(int(btc_data[x]['time'])))

print(btc_data)

#make empty lists for coin prices
btcpricelist = []

#append prices to empty lists

for x in range(0,len(btc_data) - 1):
	btcpricelist.append(btc_data[x]['close'])

    #currentprices
btcprice = btcpricelist[-1:]

print(" ")
print(btcpricelist[0])
print(btcprice)


# In[ ]:


#make a graph of daily returns
    #add price lists to dictionaries for graphing
datalist = {}
datalist["BTC"] = btcpricelist

mydata = pd.DataFrame(data=datalist)

for x in range(1, len(mydata)-1):
    daily_return_since_conception = ((mydata/mydata.iloc[0])*100)

print('BTC mean daily return since conception: ', daily_return_since_conception['BTC'].mean())


daily_return_since_conception.plot(figsize=(25,10));
plt.show()


