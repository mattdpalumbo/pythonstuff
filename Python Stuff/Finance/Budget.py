﻿#this is how we can budget every month
import PortfolioMaster

#torrie’s per month


Torrie_monthly_income = 1093.0 * 2.0
Torrie_rent = 1160.0/2.0
Torrie_car_bill = 440.0
Torrie_insurance_bill = 200.0 
Torrie_phone_bill = 130.0
torrie_Groceries = 30 * 4

#split bills per month
Internet_bill = 20.0
Electric_bill = 70.0
Water_bill =  40.0
Billy_food = 25.0


#matts bills
Matt_rent = 1160.0/2.00
Matt_monthly_income = 1102.00*2
matt_groceries = 90 * 4
matt_gas = 15*4


Torrie_pays = Torrie_rent + Torrie_car_bill + Torrie_insurance_bill + Torrie_phone_bill + Internet_bill + Electric_bill + Water_bill + Billy_food + torrie_Groceries
matt_pays = Matt_rent + Internet_bill + Electric_bill + Water_bill + Billy_food + matt_groceries +matt_gas

Torrie_balance = Torrie_monthly_income - Torrie_pays
matt_balance = Matt_monthly_income - matt_pays

print ('Torrie pays this much in bills: $%s' % Torrie_pays)

print ('So Torrie will have this much left over: $%s' % Torrie_balance)

print('')
print ('So Torrie will have this much spending a week, without saving: $%s' % (Torrie_balance/4))

print ('Matt Pays: $%s' % matt_pays)

print ('So Matt will have this much left over: $%s' % matt_balance)

matt_savings_potential = matt_balance * 0.75 
torrie_saving_potential = 160

monthly_potential_savings = (matt_savings_potential + torrie_saving_potential)

invest_ten_percent_crypt = monthly_potential_savings * 0.1

predection = monthly_potential_savings*((1 + (PortfolioMaster.daily_return_port/365.00))**365)

weighted_daily_return = PortfolioMaster.total_returns_over_time.mean()/PortfolioMaster.days_since_invested.days

weighted_predection = monthly_potential_savings*((1 + (PortfolioMaster.weighted_daily_return/365.00))**365)

educated_guess = (predection + weighted_predection) / 2

print('Torrie wants to save this much per paycheck: $%s' % str(torrie_saving_potential/2))
print("Matt wants to save this much per paycheck: $%s" % str(matt_savings_potential/2))
print('Together we could potentially save this much per month: $%s' %monthly_potential_savings)
print('')
print(" ")
print(" ")
for x in range(0,13):
	print('In %s months of saving we can potentially save $%s'% (x ,(x*monthly_potential_savings)))
print(' ')
print(' ')
print('If we invested one month of our yearly potential savings, $%s, into my crypto portfolio, each investment of %s would be potentially worth $%s a year later, at current growth.' %(monthly_potential_savings, monthly_potential_savings, educated_guess))
print('')
print('At this same rate, with the investement I have already made, before mining anything we would have a potential (possibly ambitious) savings of: $%s' % (educated_guess + PortfolioMaster.educated_guess +(monthly_potential_savings * 11)))
print('   ')


print('____________________██████')
print('_________▓▓▓▓____█████████')
print('__ Ƹ̵̡Ӝ̵̨̄Ʒ▓▓▓▓▓=▓____▓=▓▓▓▓▓')
print('__ ▓▓▓_▓▓▓▓░●____●░░▓▓▓▓')
print('_▓▓▓▓_▓▓▓▓▓░░__░░░░▓▓▓▓')
print('_ ▓▓▓▓_▓▓▓▓░░♥__♥░░░▓▓▓')
print('__ ▓▓▓___▓▓░░_____░░░▓▓')
print('▓▓▓▓▓____▓░░_____░░▓')
print('_ ▓▓____ ▒▓▒▓▒___ ████')
print('_______ ▒▓▒▓▒▓▒_ ██████')
print('_______▒▓▒▓▒▓▒ ████████')
print('_____ ▒▓▒▓▒▓▒_██████ ███')
print('_ ___▒▓▒▓▒▓▒__██████ _███')
print('_▓▓X▓▓▓▓▓▓▓__██████_ ███')
print('▓▓_██████▓▓__██████_ ███')
print('▓_███████▓▓__██████_ ███')
print('_████████▓▓__██████ _███')
print('_████████▓▓__▓▓▓▓▓▓_▒▒')
print('_████████▓▓__▓▓▓▓▓▓')
print('_████████▓▓__▓▓▓▓▓▓')
print('__████████▓___▓▓▓▓▓▓')
print('_______▒▒▒▒▒____▓▓▓▓▓▓')
print('_______▒▒▒▒▒ _____▓▓▓▓▓')
print('_______▒▒▒▒▒_____ ▓▓▓▓▓')
print('_______▒▒▒▒▒ _____▓▓▓▓▓')
print('________▒▒▒▒______▓▓▓▓▓')
print('________█████____█████')
print('_▀█║────────────▄▄───────────​─▄──▄_')
print('──█║───────▄─▄─█▄▄█║──────▄▄──​█║─█║')
print('──█║───▄▄──█║█║█║─▄║▄──▄║█║─█║​█║▄█║')
print('──█║──█║─█║█║█║─▀▀──█║─█║█║─█║​─▀─▀')
print('──█║▄║█║─█║─▀───────█║▄█║─▀▀')
print('──▀▀▀──▀▀────────────▀─█║')
print('───────▄▄─▄▄▀▀▄▀▀▄──▀▄▄▀')
print('──────███████───▄▀')
print('──────▀█████▀▀▄▀')
print('────────▀█▀')
