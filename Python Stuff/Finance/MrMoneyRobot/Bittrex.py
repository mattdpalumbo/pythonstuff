import urllib.request, json

import datetime


#get number of days since first investment date
today = datetime.date.today()
first_day_market_loaded = datetime.date(2018, 1, 2)
#days_since_invested = today - first_day_invested


#get bittrex markets

with urllib.request.urlopen("https://bittrex.com/api/v1.1/public/getmarkets") as url: 
	bittrex_markets = json.loads(url.read().decode())

market_names = []
for x in bittrex_markets['result']:
	market_names.append(x['MarketName'])

print (market_names)

	
#get bittrex coins

with urllib.request.urlopen("https://bittrex.com/api/v1.1/public/getcurrencies ") as url: 
	bittrex_coins = json.loads(url.read().decode())

coins_names = {}
for x in bittrex_coins['result']:
	coins_names[str(x['Currency'])] = str(x['CurrencyLong'])

print(" ")
print(coins_names)

with urllib.request.urlopen("https://bittrex.com/api/v1.1/public/getmarkets") as url: 
	bittrex_markets = json.loads(url.read().decode())