import urllib.request, json

import datetime


#get number of days since first investment date
today = datetime.date.today()
first_day_market_loaded = datetime.date(2018, 1, 2)
#days_since_invested = today - first_day_invested


#get bittrex markets

with urllib.request.urlopen("https://bittrex.com/api/v1.1/public/getmarkets") as url: 
	bittrex_markets = json.loads(url.read().decode())

market_names = []
for x in bittrex_markets['result']:
	market_names.append(x['MarketName'])

print (market_names)