import urllib.request, json

import datetime

with urllib.request.urlopen("https://bittrex.com/api/v1.1/public/getcurrencies") as url: 
	bittrex_coins = json.loads(url.read().decode())

coins_names = {}
for x in bittrex_coins['result']:
	coins_names[str(x['Currency'])] = str(x['CurrencyLong'])

print(" ")
print(coins_names)

