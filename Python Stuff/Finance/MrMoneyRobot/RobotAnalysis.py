
import urllib.request, json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime
#import BittrexMarkets as bm
#import BittrexCoins as bc
import time
import tkinter 
from tkinter import Tk, Label, Button

class MyFirstGUI:
    def __init__(self, master):
        self.master = master
        master.title("A simple GUI")

        self.label = Label(master, text="This is our first GUI!")
        self.label.pack()

        self.greet_button = Button(master, text="Greet", command=self.greet)
        self.greet_button.pack()

    def greet(self):
        print("Greetings!")

root = Tk()
my_gui = MyFirstGUI(root)
root.mainloop()


#get coins history on the market start with USD values will start with day, add which history feature later

with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym=USD&allData=true") as url:
    btc = json.loads(url.read().decode())

#get JUST the data, not the response confirmation

btc_data = btc["Data"]

#make empty lists for coin prices
btcpricelist = []

#append prices to empty lists

for x in range(0,len(btc_data) - 1):
	btcpricelist.append(btc_data[x]['close'])

    #currentprices
btcprice = btcpricelist[-1:]

print(" ")
print(btcprice)

#add price lists to dictionaries for graphing
datalist = {}
datalist["BTC"] = btcpricelist

mydata = pd.DataFrame(data=datalist)

for x in range(1, len(mydata)-1):
    daily_return_since_conception = ((mydata/mydata.shift(1)-1)*100)

print('BTC mean daily return since conception: ', daily_return_since_conception['BTC'].mean())

daily_return_since_conception.plot(figsize=(25,10));
#plt.show()


btc_data_redo = btc["Data"]

mynewdata = pd.DataFrame(data=btc_data_redo)

for x in range(0, len(btc_data_redo)-1):
    btc_data_redo[x]['time'] = time.strftime("%D %H:%M", time.localtime(int(btc_data_redo[x]['time'])))

print(btc_data_redo)