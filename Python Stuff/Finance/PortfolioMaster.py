import urllib.request, json
import numpy as np
import pandas as pd
from pandas_datareader import data as wb
import matplotlib.pyplot as plt
import datetime

#get number of days since first investment date
today = datetime.date.today()
first_day_invested = datetime.date(2017, 10, 30)
days_since_invested = today - first_day_invested

#print (days_since_invested)

#get jsons

		#btc
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=BTC&tsym=USD&limit=%s" % days_since_invested.days) as url:
    btc = json.loads(url.read().decode())

		#ltc
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=LTC&tsym=USD&limit=%s" % days_since_invested.days) as url:
    ltc = json.loads(url.read().decode())

		#vtc
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=VTC&tsym=USD&limit=%s" % days_since_invested.days) as url:
    vtc = json.loads(url.read().decode())

		#ETH
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=ETH&tsym=USD&limit=%s" % days_since_invested.days) as url:
    eth = json.loads(url.read().decode())

		#XLM
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=XLM&tsym=USD&limit=%s" % days_since_invested.days) as url:
    xlm = json.loads(url.read().decode())

		#sc
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=SC&tsym=USD&limit=%s" % days_since_invested.days) as url:
    sc = json.loads(url.read().decode())

		#NXS
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=NXS&tsym=USD&limit=%s" % days_since_invested.days) as url:
    nxs = json.loads(url.read().decode())

		#enj
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=ENJ&tsym=USD&limit=%s" % days_since_invested.days) as url:
    enj = json.loads(url.read().decode())

		#ethos
with urllib.request.urlopen("https://min-api.cryptocompare.com/data/histoday?fsym=BQX&tsym=USD&limit=%s" % days_since_invested.days) as url:
    ethos = json.loads(url.read().decode())


#get JUST the data, not the response confirmation
btcdata = btc['Data']
ltcdata = ltc['Data']
vtcdata = vtc['Data']
ethdata = eth['Data']
xlmdata = xlm['Data']
scdata = sc['Data']
nxsdata = nxs['Data']
enjdata = enj['Data']
ethosdata = ethos['Data']

#make empty lists for coin prices
btcpricelist = []
ltcpricelist = []
vtcpricelist = []
ethpricelist = []
xlmpricelist = []
scpricelist = []
nxspricelist = []
enjpricelist = []
ethospricelist = []



#append prices to empty lists

for x in range(0,days_since_invested.days):
	btcpricelist.append(btcdata[x]['close'])
	ltcpricelist.append(ltcdata[x]['close'])
	vtcpricelist.append(vtcdata[x]['close'])
	ethpricelist.append(ethdata[x]['close'])
	xlmpricelist.append(xlmdata[x]['close'])
	scpricelist.append(scdata[x]['close'])
	nxspricelist.append(nxsdata[x]['close'])
	enjpricelist.append(enjdata[x]['close'])
	ethospricelist.append(ethosdata[x]['close'])

for x in range(0, len(enjpricelist) - 1):
	if enjpricelist[x] == 0:
		enjpricelist[x] = 0.02345


print (enjpricelist)

#currentprices
btcprice = btcpricelist[days_since_invested.days - 1]
ltcprice = ltcpricelist[days_since_invested.days - 1]
vtcprice = vtcpricelist[days_since_invested.days - 1]
ethprice = ethpricelist[days_since_invested.days - 1]
xlmprice = xlmpricelist[days_since_invested.days - 1]
scprice = scpricelist[days_since_invested.days - 1]
nxsprice = nxspricelist[days_since_invested.days - 1]
enjprice = enjpricelist[days_since_invested.days - 1]
ethosprice = ethospricelist[days_since_invested.days - 1]

#add price lists to dictionaries for graphing

datalist = {}
datalist["BTC"] = btcpricelist
datalist["LTC"] = ltcpricelist
datalist["VTC"] = vtcpricelist
datalist["ETH"] = ethpricelist
datalist["XLM"] = xlmpricelist
datalist["SC"] = scpricelist
datalist["NXS"] = nxspricelist
datalist["ENJ"] = enjpricelist
datalist["ETHOS"] = ethospricelist	

mydata = pd.DataFrame(data=datalist)

sec_returns = ((mydata/mydata.shift(1)-1)*100)
print('BTC mean daily change: ',sec_returns['BTC'].mean())
print('LTC mean daily change: ',sec_returns['LTC'].mean())
print('VTC mean daily change: ',sec_returns['VTC'].mean())
print('ETH mean daily change: ',sec_returns['ETH'].mean())
print('XLM mean daily change: ',sec_returns['XLM'].mean() )
print('SC mean daily change: ',sec_returns['SC'].mean())
print('NXS mean daily change: ',sec_returns['NXS'].mean())
print('ENJ mean daily change: ',sec_returns['ENJ'].mean())
print('ETHOS mean daily change: ', sec_returns['ETHOS'].mean())

#sec_returns.plot(figsize=(25,10));
#plt.show()

daily_return_port = float(sum(sec_returns.mean())/9)


print("Mean portfolio daily return: ", daily_return_port)
print(' ')


#where you input your coin balance
enjamount = 420.00
abjamount = 0.00
vtcamount = 80
btcamount  = 0
ltcamount  = 0.0
xlmamount  = 1248
ethosamount  = 39.9505226
neoamount  = 0.0
nxsamount  = 25.0
ethamount  = 0.0
scamount  = 9040.42

#your total current balance
my_balance = float((vtcprice * vtcamount) 
	+ (btcamount * btcprice) 
	+ (ltcamount * ltcprice)
	+ (xlmamount * xlmprice)
	+ (ethosamount * ethosprice)
	+ (nxsamount * nxsprice)
	+ (ethprice * ethamount)
	+ (scprice * scamount)
	+ (enjprice * enjamount)
	)

#coinbalances
btcbalance = (btcamount * btcprice)
ltcbalance = (ltcamount * ltcprice)
vtcbalance = (vtcprice * vtcamount)
xlmbalance = (xlmamount * xlmprice)
ethosbalance = (ethosamount * ethosprice)
nxsbalance = (nxsamount * nxsprice)
ethbalance = (ethprice * ethamount)
scbalance = (scprice * scamount)
enjbalance = (enjprice * enjamount)

#your total investment amount in USD
initial_capital = 10.00 + 20.00 + 50.00 + 100.00  + 100.00 + 50.00 + 45.00 + 78.00 + 50.00 + 59.77 + 59.70 + 50.00 + 150 + 200

#your break even on investment withdrawn
withdrawn_usd = 517.18 + 83.00 + 75.01 + 147.28 + 100

#this capital is how much you're in the hole
capital = initial_capital - withdrawn_usd

#these are obvious
percent_change = ((my_balance/capital) * 100.00) -  100
current_profit = my_balance - capital
in_the_clear = initial_capital - withdrawn_usd




#print data
print("")
print("")
print("")
print(("My balance is:"), my_balance)

print(("My initial capital was:"), initial_capital)
print("My total withdrwals in USD have been: $", withdrawn_usd)
print(("My current net invested capital is:"), format(capital, '.2f'))
print("")
print (("My current profit not considering profit withdrawn USD is:"), current_profit)

print ("")
if capital < 0.01:
	print ("The percent change is: INIFINITY, EVERYTHING IS PROFIT BOYEEEEE!!!!!")
else:
	print (("The percent change is: "), percent_change)


print ("")
print  ("BTC value: $", (btcamount * btcprice))
print  ("VTC value: $", (vtcamount * vtcprice))
print ("LTC value: $", (ltcamount * ltcprice))
print ("XLM value: $", (xlmamount * xlmprice))
print ("Ethos value: $", (ethosamount * ethosprice))
#print ("NEO value: $", (neoprice * neoamount))	
print ("NXS value: $", (nxsamount * nxsprice))
print ("ETH value: $", (ethprice * ethamount))
print ("SC value: $", (scprice * scamount))
print ("ENJ value: $", (enjprice * enjamount))
print ("")
print ("I have withdrawn, ", withdrawn_usd," in USD. That leaves $", format(in_the_clear, ".2f"), " until I'm in the clear and everything is profit")
print("")

print("Put Calclulations down here bub:")
print("")

#weight at time of calculation
weights = {}

#weights["BTC"] = (btcbalance/my_balance) * 100
#weights["LTC"] = (ltcbalance/my_balance) * 100
weights["VTC"] = (vtcbalance/my_balance) * 100
#weights["ETH"] = (ethbalance/my_balance) * 100
weights["XLM"] = (xlmbalance/my_balance) * 100
weights["SC"] = (scbalance/my_balance) * 100
weights["NXS"] = (nxsbalance/my_balance) * 100
weights["ENJ"] = (enjbalance/my_balance) * 100
weights["ETHOS"] = (ethosbalance/my_balance) * 100


#print("Weights: %s" % weights)


#balance over time since initial investment
#does not account for coins added between now and first day
#this is more for an idea of how portfolio is/would have grown
#PERHAPS TODO - account for midway coins

#all the below until otherwise said is  for return rate over time

balancelist = {}
balancelist["BTC"] = np.multiply(btcpricelist ,btcamount)
balancelist["LTC"] = np.multiply(ltcpricelist , ltcamount)
balancelist["VTC"] = np.multiply(vtcpricelist , vtcamount)
balancelist["ETH"] = np.multiply(ethpricelist , ethamount)
balancelist["XLM"] = np.multiply(xlmpricelist , xlmamount)
balancelist["SC"] = np.multiply(scpricelist , scamount)
balancelist["NXS"] = np.multiply(nxspricelist , nxsamount)
balancelist["ENJ"] = np.multiply(enjpricelist , enjamount)
balancelist["ETHOS"] = np.multiply(ethospricelist , ethosamount)


balance_over_time = (balancelist["BTC"] + 
	balancelist["LTC"] + 
	balancelist["VTC"] + 
	balancelist["ETH"] + 
	balancelist["XLM"] +
	balancelist["SC"] +
	balancelist["NXS"] + 
	balancelist["ENJ"] + 
	balancelist["ETHOS"] )

balancelist["Total Balance"] = balance_over_time

#This is the graph of % returns since the first day I invested in crypto

balance_data = pd.DataFrame(data=balancelist)
((balance_data/balance_data.iloc[0] - 1) * 100).plot(figsize=(55,10));
plt.xlabel('Days Since Investment: {0} to {1}'.format(datetime.date(2017, 10, 30), datetime.date.today()))
plt.ylabel('Precent Change')
plt.title('Daily Percent Change in Coins')


cov_matrix = ((balance_data/balance_data.iloc[0] - 1) * 100).cov()

#print(cov_matrix)

#print ((balance_data/balance_data.iloc[0] - 1) * 100)

weights_over_time = {}
weights_over_time["BTC"] = np.divide(balancelist["BTC"], balance_over_time) 
weights_over_time["LTC"] = np.divide(balancelist["LTC"], balance_over_time) 
weights_over_time["VTC"] = np.divide(balancelist["VTC"], balance_over_time) 
weights_over_time["ETH"] = np.divide(balancelist["ETH"], balance_over_time) 
weights_over_time["XLM"] = np.divide(balancelist["XLM"], balance_over_time) 
weights_over_time["SC"] = np.divide(balancelist["SC"], balance_over_time) 
weights_over_time["NXS"] = np.divide(balancelist["NXS"], balance_over_time) 
weights_over_time["ENJ"] = np.divide(balancelist["ENJ"], balance_over_time) 
weights_over_time["ETHOS"] = np.divide(balancelist["ETHOS"], balance_over_time) 

#print("Weights over time: %s" % weights_over_time)
wot_data  = pd.DataFrame(data=weights_over_time)
#wot_data.plot(figsize=(10,10));


returns_over_time = {}
returns_over_time["BTC"] = (np.divide(balancelist["BTC"], balancelist["BTC"][0])-1) * 100
returns_over_time["LTC"] = (np.divide(balancelist["LTC"], balancelist["LTC"][0])-1) * 100
returns_over_time["VTC"] = (np.divide(balancelist["VTC"], balancelist["VTC"][0])-1) * 100
returns_over_time["ETH"] = (np.divide(balancelist["ETH"], balancelist["ETH"][0])-1)* 100
returns_over_time["XLM"] = (np.divide(balancelist["XLM"], balancelist["XLM"][0])-1) * 100
returns_over_time["SC"] = (np.divide(balancelist["SC"], balancelist["SC"][0])-1) * 100
returns_over_time["NXS"] = (np.divide(balancelist["NXS"], balancelist["NXS"][0]) -1)* 100
returns_over_time["ENJ"] = (np.divide(balancelist["ENJ"], balancelist["ENJ"][0])-1) * 100
returns_over_time["ETHOS"] = (np.divide(balancelist["ETHOS"], balancelist["ETHOS"][0])-1)* 100

#print (returns_over_time)

total_returns_over_time = (
	#(returns_over_time["BTC"] * weights_over_time["BTC"])+ 
	#(returns_over_time["LTC"] * weights_over_time["LTC"] )+ 
	(returns_over_time["VTC"] * weights_over_time["VTC"])+ 
	#(returns_over_time["ETH"] * weights_over_time["ETH"]) + 
	(returns_over_time["XLM"] * weights_over_time["XLM"] )+
	(returns_over_time["SC"] * weights_over_time["SC"] )+
	(returns_over_time["NXS"] * weights_over_time["NXS"])+ 
	(returns_over_time["ENJ"] * weights_over_time["ENJ"] )+ 
	(returns_over_time["ETHOS"] * weights_over_time["ETHOS"]))

trot_data = pd.DataFrame(data=total_returns_over_time)
trot_data.plot(figsize=(55,10));
plt.axis([0,days_since_invested.days, min(total_returns_over_time), max(total_returns_over_time)])

predection = my_balance*((1 + (daily_return_port/365.00))**365)

weighted_daily_return = total_returns_over_time.mean()/days_since_invested.days

weighted_predection = my_balance*((1 + (weighted_daily_return/365.00))**365)

educated_guess = (predection + weighted_predection) / 2

print("At the current daily return rate I should have $ %s if I HODL for 365 days, given the the individual coins history since purchase" % predection)
print("")
print("At the current weighted daily return rate I should have $ %s if I HODL for 365 days, given my portfolio's history and weight change over time" % weighted_predection)
print("")
print("So maybe an educated guess would be somewhere around $ %s if I HODL for a 365 days" % educated_guess )


#btcltc_cov = ((returns_over_time["BTC"] - sec_returns["BTC"].mean()) * (returns_over_time["LTC"] - sec_returns["LTC"].mean()))/54
#btcltc_cov_data= pd.DataFrame(data=btcltc_cov)
#btcltc_cov_data.plot(figsize=(55,10));


plt.show()

#how_much_each_to_withdraw = weights_over_time.keys() * in_the_clear

print("")


def withdraw_from_each(usdamount):
	for x in weights:
		print("To withdraw evenly, withdraw this much of this amount (more coins below): %s - %s " % (x, ((usdamount * (weights[x]/100))/datalist[x][int(days_since_invested.days) -1] )))

withdraw_from_each(1200)
	