import pygame, sys, time
from pygame.locals import *

#         R    G    B             <---- Color map
WHITE = (255, 255, 255)
BLACK = (000, 000, 000)



# 1 2 3 4 5 6 


pygame.init()

FPS = 30 #frames per second setting
fpsClock = pygame.time.Clock()

#set up the window
DISPLAYSURF = pygame.display.set_mode( (0,0), 0, 0)
pygame.display.set_caption('Move the square')


catImg = pygame.image.load('cat.png')
catx = 10
caty = 10 
direction = 'right'
boxx = 25
boxy = 25
moveby = (15, 15)
keys =  pygame.key.get_pressed()
move_ticker = 0

while True: #the main game loop

	DISPLAYSURF.fill(WHITE)
	square = pygame.draw.rect(DISPLAYSURF, BLACK, (960, 590, boxx, boxy))

	if direction == 'right':
		catx += 5
		if catx == 280:
			direction = 'down'
	elif direction == 'down':
		caty+= 5
		if caty == 220:
			direction = 'left'
	elif direction == 'left':
		catx -= 5
		if catx == 10:
			direction = 'up'
	elif direction == 'up':
		caty -= 5
		if caty == 10:
			direction = 'right'



	DISPLAYSURF.blit(catImg, (catx, caty))

	for event in pygame.event.get():
		if event.type == QUIT:
			pygame.quit()
			sys.exit()

		if event.type == pygame.KEYDOWN:
			square.move(0, 10)
		

	pygame.display.update()
	fpsClock.tick(FPS)

